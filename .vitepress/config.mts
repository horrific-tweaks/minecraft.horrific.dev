import { defineConfig, HeadConfig } from 'vitepress'

const constants = {
  name: "Horrific Tweaks",
  description: "A collection of various Minecraft minigames and other projects.",
}

// https://vitepress.dev/reference/site-config
export default defineConfig({
  lang: "en-US",
  title: constants.name,
  description: constants.description,
  head: [
    ["meta", { property: "name", content: constants.name }],
    ["meta", { property: "og:locale", content: "en" }],
    ["meta", { property: "og:site_name", content: constants.name }],
  ],
  cleanUrls: true,
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Wiki', link: '/games/' },
    ],

    sidebar: [
      {
        text: 'Games',
        collapsed: false,
        items: [
          { text: 'Horrific Minigames', link: '/games/' },
        ],
      },
      {
        text: 'Packs',
        collapsed: false,
        items: [
          { text: 'Resource / Data Packs', link: '/packs/' },
          { text: 'Installation Help', link: '/wiki/Installing' },
        ],
      },
    ],

    socialLinks: [
      { icon: { svg: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>gitlab</title><path d=\"M21.94 13.11L20.89 9.89C20.89 9.86 20.88 9.83 20.87 9.8L18.76 3.32C18.65 3 18.33 2.75 17.96 2.75C17.6 2.75 17.28 3 17.17 3.33L15.17 9.5H8.84L6.83 3.33C6.72 3 6.4 2.75 6.04 2.75H6.04C5.67 2.75 5.35 3 5.24 3.33L3.13 9.82C3.13 9.82 3.13 9.83 3.13 9.83L2.06 13.11C1.9 13.61 2.07 14.15 2.5 14.45L11.72 21.16C11.89 21.28 12.11 21.28 12.28 21.15L21.5 14.45C21.93 14.15 22.1 13.61 21.94 13.11M8.15 10.45L10.72 18.36L4.55 10.45M13.28 18.37L15.75 10.78L15.85 10.45H19.46L13.87 17.61M17.97 3.94L19.78 9.5H16.16M14.86 10.45L13.07 15.96L12 19.24L9.14 10.45M6.03 3.94L7.84 9.5H4.23M3.05 13.69C2.96 13.62 2.92 13.5 2.96 13.4L3.75 10.97L9.57 18.42M20.95 13.69L14.44 18.42L14.46 18.39L20.25 10.97L21.04 13.4C21.08 13.5 21.04 13.62 20.95 13.69\" /></svg>" }, link: "https://gitlab.com/horrific-tweaks/minecraft.horrific.dev" },
      { icon: "discord", link: "https://discord.horrific.dev" },
    ]
  },
  transformHead({ pageData }) {
    const head: HeadConfig[] = [];

    if (!pageData.isNotFound) {
      const canonicalUrl = `https://minecraft.horrific.dev/${pageData.relativePath}`
        .replace(/\.md$/, '')
        .replace(/\/index$/, '/');

      head.push(['link', { rel: 'canonical', href: canonicalUrl }]);
      head.push(['meta', { property: 'og:url', content: canonicalUrl }]);

      const formattedTitle = pageData.title
        ? (
          canonicalUrl.endsWith("/")
            ? pageData.title
            : `${pageData.title} | ${constants.name}`
        )
        : constants.name;
      const description = pageData.description || constants.description;

      head.push(['meta', { name: 'description', content: description }]);
      head.push(['meta', { property: 'og:title', content: pageData.title || constants.name }]);
      head.push(['meta', { property: 'og:description', content: description }]);
      head.push(['meta', { property: 'og:type', content: canonicalUrl.endsWith("/") ? "website" : "article" }]);
      head.push(['meta', { property: 'twitter:title', content: formattedTitle }]);
      head.push(['meta', { property: 'twitter:description', content: description }]);
    }

    return head;
  },
  srcExclude: ["repositories"],
})
