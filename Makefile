.vitepress/dist: public public/bingo *.md .vitepress scripts/*
	npm ci --cache ~/.npm --prefer-offline
	npm run docs:build
	npm run sitemap

public/bingo: repositories/bingo/docs
	cd repositories/bingo/docs; \
		npm ci --cache ~/.npm --prefer-offline; \
		npm run docs:build
	cp -r repositories/bingo/docs/.vitepress/dist public/bingo
