import _packs from './packs.json';

export function titleize(str: string) : string {
	return str.split(/[_-]/)
		.map(w => w[0].toUpperCase() + w.substring(1))
		.join(" ")
		.replace(/([a-z])([A-Z])/g, "$1 $2")
		.replace(/([A-Z])([A-Z][a-z])/g, "$1 $2");
}

export const versions = ["1.17", "1.18", "1.19", "1.20"];

export type PackInfo = {
	name: string;
	title: string;
	description: string;
	html_url: string;
	raw_url: string;
	download_url: string;
	tag_name: string;
	types: string[];
	supports: string[];
};

async function fetchPack(url: string) : Promise<PackInfo> {
	const name = url.split('/').slice(-1)[0];
	const raw_url = `https://gitlab.com/api/v4/projects/${encodeURIComponent("horrific-tweaks/" + name)}/repository/files/$$/raw`;
	const download_url = `https://horrific-tweaks.gitlab.io/${name}/$$.zip`;
	const pkg = await fetch(raw_url.replace("$$", "package.json")).then(r => r.json());

	const tag_name = pkg.version || "";
	const types = pkg.config?.type ? [pkg.config.type] : ["datapack", "resourcepack"];
	const supports = pkg.config?.supports || [pkg.config?.format || versions.slice(-1)[0]];

	return {
		name: pkg.name,
		title: titleize(pkg.name),
		description: pkg.description || "",
		html_url: url,
		raw_url,
		download_url,
		tag_name,
		types,
		supports,
	};
}

let packs: PackInfo[] = [];
export async function fetchPacks() : Promise<PackInfo[]> {
	if (packs.length) return packs;

	// construct array of pack info
	const ret: PackInfo[] = [];
	for (const url of _packs) {
		const pack = await fetchPack(url)
			.catch(e => console.error(`Error fetching ${url}:`, e));

		if (pack)
			ret.push(pack);
	}

	packs = ret;
	return ret;
}
