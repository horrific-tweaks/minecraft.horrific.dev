# Horrific Minigames

We're building a set of tournament-based minigames for recent Minecraft versions!

If you're interested in beta testing these as they're being developed, sign up in [our Discord](https://discord.horrific.dev)!

::: info [Yet Another Minecraft BINGO](/bingo/)
A multiplayer BINGO item hunt mod + lobby, compatible with vanilla clients.
:::

::: info [Endless Parkour](https://gitlab.com/horrific-tweaks/endless-parkour)
An infinite, tiled parkour map with various gamemodes and endless progression.
:::

::: info [Chaotic Caving](https://gitlab.com/horrific-tweaks/chaotic-caving)
A timed spelunking adventure where the objective is to cave for special ores in a custom dimension.

Find unique boss fights, loot boxes, and custom equipment scattered about the world!
:::

## Credits

- [fennifith](https://twitch.tv/fennifith): Author
- [Crutchcorn](https://twitch.tv/crutchcorn): Structure Design & Playtesting
- [Carmen_the_Frog](https://twitch.tv/carmen_the_frog): Structure Design & Playtesting
- bort_theboi: Structure Design & Playtesting
- [thegroose](https://twitter.com/thegroose): Structure Design & Playtesting
- [Xenophorium](https://card.xenophorium.dev): Structure Design & Playtesting
- [Dewlan](https://twitch.tv/dewlantv): Playtesting
- Xenia725: Playtesting
- Bader: Playtesting
- [bearikeen](https://twitch.tv/bearikeen): Playtesting
- sitting_duck: Playtesting
- MusicGoat: Playtesting
