---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Horrific Tweaks"
  text: ""
  tagline: A collection of various Minecraft minigames and other projects.
  image:
    src: /logo.png
    alt: Horrific Dev Logo
  actions:

features:
  - title: Yet Another Minecraft BINGO
    details: A server-side BINGO item hunt mod + lobby, compatible with vanilla clients.
    link: https://minecraft.horrific.dev/bingo/
    icon:
      src: /bingo/favicon.svg
  - title: Coming soon...
---

