---
layout: doc
---

# Resource / Data Packs

This is a random collection of minimally game-altering Minecraft resource packs & datapacks.

Some items will have both a resource pack and datapack file. These both need to be installed for them to work as expected.

[Installation Help](/wiki/Installing)

<script setup>
	import PackList from "../components/PackList.vue";
</script>

<PackList />

