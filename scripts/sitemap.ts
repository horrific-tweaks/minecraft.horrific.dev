import fs, { promises as fsp } from "fs";
import path from "path";
import { SitemapStream } from "sitemap";

async function getFiles(dir): Promise<string[]> {
	const ret: string[] = [];

	for (const file of await fsp.readdir(dir)) {
		const absoluteFile = path.resolve(dir, file);
		const relativeFile = path.join(dir, file);
		const fileStat = await fsp.stat(absoluteFile);
		if (fileStat.isDirectory()) {
			ret.push(
				...(await getFiles(relativeFile))
			);
		} else {
			ret.push(relativeFile);
		}
	}

	return ret;
}

const sitemap = new SitemapStream({ hostname: "https://minecraft.horrific.dev" });
const writeStream = fs.createWriteStream(".vitepress/dist/sitemap.xml");
sitemap.pipe(writeStream);

const paths = (await getFiles(".vitepress/dist"))
	.filter(file => file.endsWith(".html"))
	.filter(file => !file.endsWith("404.html"))
	.map(file => file.replace(".vitepress/dist", ""))
	.map(file => file.replace("index.html", ""))
	.map(file => file.replace(/\.html$/, ""));

for (const path of paths) {
	sitemap.write({
		url: path,
		changefreq: 'daily',
		lastmod: new Date().toISOString(),
		priority: 0.7,
	});
}

sitemap.end();
