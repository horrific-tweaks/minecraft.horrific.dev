First, go to the [Horrific Tweaks](https://minecraft.horrific.dev/) homepage and select the packs you want to install. You can pick as many as you want - or click on "About" to download the packs individually.

## Resource Packs

After opening Minecraft, click on "Options" in the main menu.

Choose "Resource Packs" in the options screen.

Click on "Open Resource Pack Folder." This should open the "resourcepacks" folder in your file explorer.

Once you have downloaded a resource pack zip, you can drag it into this folder.

The resource pack should then appear in Minecraft. To enable it, click on the resource pack under the "Available Resource Packs" section to move it into "Selected Resource Packs."

## Datapacks

After opening Minecraft, click on "Single Player" in the main menu.

Find the world where you want to install the datapack, select it, and press "Edit".

Choose "Open World Folder" in the Edit menu. This should open the folder in your file explorer.

Go into the "datapacks" folder.

Once you have downloaded a datapack, you can drag it into this folder. The datapack will be applied the next time you open the world.

### Realms / Multiplayer

For multiplayer worlds, the datapack will need to be placed in the `world/datapacks/` folder. If you are using Realms, you will need to [download your world from Realms](https://help.minecraft.net/hc/en-us/articles/360035130891-Minecraft-Java-Edition-How-Do-I-Download-My-Realms-World-), then re-upload it after the datapack is added.

If you are running a multiplayer server, you will need to either run `/reload` or restart the server after adding the datapack.
